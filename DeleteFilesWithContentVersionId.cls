
public without sharing class DeleteFilesWithContentVersionId {
    @AuraEnabled
    public static String deleteContentVersion(String contentVersionId){
      //  ContentVersion newConterntVersion = new ContentVersion();
        // ContentDocument doc = new ContentDocument();
         List<ContentDocument> ContentDocumentList = new List<ContentDocument>();
        try {
            List<ContentVersion> contentversionIdList =new  List<ContentVersion>();
            contentversionIdList=[SELECT Id,contentDocumentId FROM contentversion where Id=:contentVersionId];
            // SELECT Id,contentDocumentId FROM contentversion where ID='068r00000039yxTAAQ'
           
            if(contentversionIdList.size()>0){

                String contentDocumnetIdVar =contentversionIdList[0].contentDocumentId;

                for(ContentDocument conDoc : [SELECT Id FROM ContentDocument WHERE Id =: contentDocumnetIdVar]){
                    ContentDocumentList.add(conDoc);
                }
                //Update Content Document Name
                delete ContentDocumentList;
                return 'deleteSuccess';
            }
            return 'error';
        } catch(DMLException ex) {
            return ex.getDMLMessage(0);
        }
       
    }
}